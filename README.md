# example-acceptance-tests

###Please note due to issues with our selenium grid current Internet Explorer 11 tests need to be run locally (When pushing your code to jenkins you need to exclude InternetExplorerTests in the failsafe plugin in the pom file)

####Requirements:
JDK 1.8 or later

####How To Run: 

######From IDE

Due to the webdriver grid to run this from your ide you would have to edit the test class configuations to add the following command line arguments:

-DlocalFlag=True

######From cmd

To build and run:
`mvn clean install -DlocalFlag=True`
  
This should build the project and launch the tests in parallel against Google Chrome, Mozilla Firefox and Internet Explorer 11.
Unless the exclude entry in the pom has been uncommented. If the exclude entry has been uncommented then IE tests will not run


#### Any questions please contact alex.walker@unilever.com

