Feature: a cucumber example feature

  Scenario: As a tester doing some automation tests I want to see an example feature
    Given I need something in a particular state
    When I do something
    Then something should happen
