package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.ExamplePageObject;

public class ExampleStepDef {
    private ExamplePageObject examplePageObject = new ExamplePageObject();

    @Given("^I need something in a particular state$")
    public void i_need_something_in_a_particular_state() {
        examplePageObject.setSomethingUp();
    }

    @When("^I do something$")
    public void i_do_something() {
        examplePageObject.doSomeAction();
    }

    @Then("^something should happen$")
    public void something_should_happen() {
        examplePageObject.checkResult();
    }
}
