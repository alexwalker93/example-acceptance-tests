package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ExamplePageObject extends AbstractPageObject {
    private static final Logger LOGGER = Logger.getLogger(ExamplePageObject.class.getName());
    //These are selectors for elements on the page
    @FindBy(className = "test")
    private WebElement test;

    //This factory class instantiates all of the elements above so that they can be interacted with
    public ExamplePageObject() {
        PageFactory.initElements(driver, this);
    }

    @Override
    public void go() {
        final String url = "http://54.229.47.209:8080/createSitePage";
        goTo(url);
    }

    public void setSomethingUp() {
        LOGGER.log(Level.INFO, "Setting something up");
    }

    public void doSomeAction() {
        LOGGER.log(Level.INFO, "Doing some Action");
    }

    public void checkResult() {
        LOGGER.log(Level.INFO, "Checking the result");
    }
}
