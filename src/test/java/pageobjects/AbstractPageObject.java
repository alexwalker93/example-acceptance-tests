package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.BrowserDriver;

import java.net.MalformedURLException;
import java.time.Duration;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.System.getProperty;
import static utils.BrowserDriver.getDriver;

public abstract class AbstractPageObject {
    private static final Logger LOGGER = Logger.getLogger(BrowserDriver.class.getName());

    static WebDriver driver;

    static {
        try {
            driver = getDriver(getProperty("driverType"), getProperty("localFlag"));
        } catch (MalformedURLException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    void isWebElementVisible(WebElement webElementToBeVisible, int secondsToWait) {
        WebDriverWait waitForElement = new WebDriverWait(driver, secondsToWait);
        waitForElement.until(ExpectedConditions.visibilityOf(webElementToBeVisible));
    }

    public void waitUntilElementIsVisible(int totalTimeInSeconds, int pollingTimeInSeconds, WebElement element) {
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(totalTimeInSeconds))
                .pollingEvery(Duration.ofSeconds(pollingTimeInSeconds))
                .ignoring(org.openqa.selenium.NoSuchElementException.class);

        fluentWait.until(ExpectedConditions.visibilityOf(element));
    }

    public abstract void go();

    void goTo(String url) {
        LOGGER.log(Level.INFO, () -> "Directing browser to:" + url);
        LOGGER.log(Level.INFO, () -> "try to loadPage [" + url + "]");
        driver.get(url);
    }

    void selectDropdown(WebElement dropDown, String value) {
        Select select = new Select(dropDown);
        select.selectByVisibleText(value);
    }
}
