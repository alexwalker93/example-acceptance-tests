package utils.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.util.Properties;


@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = {"src/test/resources/features"},
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber/internetExplorer.json"},
        monochrome = true,
        tags = {"~@future", "~@ignore"},
        glue = {"stepdefs"})

public class InternetExplorerTests {

    private InternetExplorerTests() {
        throw new IllegalStateException("Utility Class");
    }

    @BeforeClass
    public static void setBrowser() {
        Properties props = System.getProperties();
        props.setProperty("driverType", "InternetExplorer");
    }
}
