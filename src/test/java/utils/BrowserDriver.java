package utils;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import pageobjects.AbstractPageObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

public abstract class BrowserDriver extends AbstractPageObject {

    private static final String PLATFORM = "Platform";
    private static final String GRID_HUB = "http://10.214.16.178:4444/wd/hub";

    private BrowserDriver() {
        throw new IllegalStateException("Utility Class");
    }

    private static final Logger LOGGER = Logger.getLogger(BrowserDriver.class.getName());
    private static WebDriver driver;

    public static synchronized WebDriver getDriver(String driverType, String localFlag) throws MalformedURLException {
        if (localFlag.equals("True")) {
            driver = startLocalDriver(driverType);
        } else {
            driver = startGridDriver(driverType);
        }
        return driver;
    }

    private static synchronized WebDriver startLocalDriver(String driverType) throws MalformedURLException {
        if (driverType.equals("Firefox")) {
            try {
                LOGGER.info("Creating firefox web driver");

                System.setProperty("webdriver.gecko.driver", "src/geckodriver.exe");
                driver = new FirefoxDriver();
                driver.manage().window().fullscreen();

            } catch (WebDriverException e) {
                LOGGER.severe(e.getMessage());
            } finally {
                Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
            }
        } else if (driverType.equals("Chrome")) {
            try {
                LOGGER.info("Creating chrome web driver");

                System.setProperty("webdriver.chrome.driver", "src/chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().fullscreen();

            } catch (WebDriverException e) {
                LOGGER.severe(e.getMessage());
            } finally {
                Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
            }
        } else if (driverType.equals("InternetExplorer")) {
            try {
                LOGGER.info("Creating IE web Driver");

                System.setProperty("webdriver.ie.driver", "src/IEDriverServer.exe");

                InternetExplorerOptions capabilities =  new InternetExplorerOptions();
                capabilities.setCapability(PLATFORM, "WIN8");

                driver = new InternetExplorerDriver(capabilities);
                driver.manage().window().maximize();

            } catch (WebDriverException e) {
                LOGGER.severe(e.getMessage());
            }
        }
        return driver;
    }

    private static synchronized WebDriver startGridDriver(String driverType) throws MalformedURLException {
        if (driverType.equals("Firefox")) {
            try {
                LOGGER.info("Creating firefox web driver");

                FirefoxOptions options = new FirefoxOptions();
                options.setCapability(PLATFORM, Platform.WIN8);
                options.setCapability("Version", 58);
                options.setCapability("marionette", true);

                driver = new RemoteWebDriver(new URL(GRID_HUB), options);

            } catch (WebDriverException e) {
                LOGGER.severe(e.getMessage());
            } finally {
                Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
            }
        } else if (driverType.equals("Chrome")) {
            try {
                LOGGER.info("Creating chrome web driver");

                ChromeOptions caps = new ChromeOptions();
                caps.setCapability(PLATFORM, Platform.WIN8);

                driver = new RemoteWebDriver(new URL(GRID_HUB), caps);

            } catch (WebDriverException e) {
                LOGGER.severe(e.getMessage());
            } finally {
                Runtime.getRuntime().addShutdownHook(new Thread(new BrowserCleanup()));
            }
        } else if (driverType.equals("InternetExplorer")) {
            try {
                LOGGER.info("Creating IE web Driver");

                InternetExplorerOptions options = new InternetExplorerOptions();
                options.setCapability(PLATFORM, Platform.WINDOWS);
                options.setCapability("Version", 11);

                driver = new RemoteWebDriver(new URL(GRID_HUB), options);
                driver.manage().window().maximize();

            } catch (WebDriverException e) {
                LOGGER.severe(e.getMessage());
            }
        }
        return driver;
    }

    private static class BrowserCleanup implements Runnable {
        @Override
        public void run() {
            try {
                driver.quit();
                LOGGER.info("closing the browser");
            } catch (UnreachableBrowserException e) {
                LOGGER.info("cannot close browser: unreachable browser");
            }
        }
    }
}
